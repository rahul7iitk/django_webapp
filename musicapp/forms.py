from django.contrib.auth.models import User
from django import forms

class UserForm(forms.ModelForm):
	password = forms.CharField(widget = forms.PasswordInput) # must specify your password field and widget is for hiding the password with dots

	class Meta: # information about the class
	    model = User
	    fields = ['username' , 'email' , 'password']

