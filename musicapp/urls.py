from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.IndexView.as_view() , name = 'index'),
    url(r'^register/$', views.register, name='register'),
    url(r'^login_user/$', views.login_user, name='login_user'),
    url(r'^logout_user/$', views.logout_user, name='logout_user'),
    url(r'^songs_all/$', views.IndexViewSong.as_view() , name = 'index_song'),
    url(r'^(?P<pk>\d+)/$' , views.DetailView.as_view() , name = 'details'),
    url(r'^album_form/$', views.CreateAlbum.as_view() , name = 'add_album'),
    url(r'^(?P<pk>\d+)/update-album/$' , views.UpdateAlbum.as_view() , name = 'update_album'),
    url(r'^(?P<pk>\d+)/delete-album/$' , views.DeleteAlbum.as_view() , name = 'delete_album'),
    url(r'^(?P<pk>\d+)/delete-song/$' , views.DeleteSong.as_view() , name = 'delete_song'),
    url(r'^song_form/$', views.CreateSong.as_view() , name = 'add_song'),
    url(r'^search/', views.search_song , name = 'search_song'),

    
]