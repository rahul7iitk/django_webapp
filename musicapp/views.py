from django.shortcuts import render , HttpResponse , Http404 ,get_object_or_404 , redirect
from django.contrib.auth import authenticate , login , logout
from .models import Album , Song
from django.views import generic
from django.views.generic import View
from django.views.generic.edit import CreateView , UpdateView , DeleteView
from django.core.urlresolvers import reverse_lazy ,reverse
from .forms import UserForm
from django.contrib.auth.decorators import login_required 

# Create your views here.

class IndexView(generic.ListView):
    
    template_name = 'musicapp/index.html'
    context_object_name = 'albums'
    def get_queryset(self):
        return Album.objects.all()

class DetailView(generic.DetailView):
    
    model = Album
    template_name = 'musicapp/details.html'

class CreateAlbum(CreateView):
    
    model = Album #new Album object
    fields = ['artist' , 'album_title' , 'genre' , 'album_logo'] #mention the fields you want in your form 

class UpdateAlbum(UpdateView):
    
    model = Album #new Album object
    fields = ['artist' , 'album_title' , 'genre' , 'album_logo'] #mention the fields you want in your update form

class DeleteAlbum(DeleteView):
    
    model = Album #new Album object
    success_url = reverse_lazy('index') #reverse_lazy redirects you to url (here 'index') after deleting your object 

class DeleteSong(DeleteView):
    
    model = Song 
    success_url = reverse_lazy('index')

class CreateSong(CreateView):

    model = Song #new Song object
    fields = ['album','song_title' , 'file' ,]
    

class IndexViewSong(generic.ListView):
    
    template_name = 'musicapp/index_song.html'
    context_object_name = 'Songs'
    def get_queryset(self):
        return Song.objects.all() 

class UserFormView(View):
    form_class = UserForm # for what class do you want to make forms
    template_name = 'musicapp/register_form.html'

    def get(self , request): # built in methods for dealing with empty forms- 'get' type
        form = self.form_class(None) # no information required for displaying empty forms
        return render(request , self.template_name , {'form':form})

        
    def post(self , request):  # built in method for dealing with filled forms (account already created)
        form = self.form_class(request.POST) # POST has actually the entire information that is submitted in the form

        if  form.is_valid():
            user = form.save(commit = False) # this basically creates an user object but has not still saved it in the database
            # this is for cleaned and normalized data
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            # passwords in django are not stored raw , but they are encrypted and stored ,so they can't be stored directly
            user.set_password(password)
            user.save()

            # form authentication
            user = authenticate(username = username , password = password)
            if  user is not None :
                if  user.is_active:
                    login(request , user)
                    # access fields such as request.user.username , request.user.profile_photo etc..
                    return redirect('musicapp:index') # go to home page
                    

        return render(request , self.template_name , {'form':form}) 

def logout_user(request):
    logout(request)
    form = UserForm(request.POST or None)
    context = {
        "form": form,
    }
    return render(request, 'musicapp/login.html', context)


def login_user(request):
    
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.all()
                return render(request, 'musicapp/index.html', {'albums': albums})
            else:
                return render(request, 'musicapp/login.html', {'error_message': 'Your account has been disabled'})
        else:
            return render(request, 'musicapp/login.html', {'error_message': 'Invalid login'})
    return render(request, 'musicapp/login.html')


def register(request):
    form = UserForm(request.POST or None)
    if form.is_valid():
        user = form.save(commit=False)
        username = form.cleaned_data['username']
        password = form.cleaned_data['password']
        user.set_password(password)
        user.save()
        user = authenticate(username=username, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                albums = Album.objects.all()
                return render(request, 'musicapp/index.html', {'albums': albums})
    context = {
        "form": form,
    }
    return render(request, 'musicapp/register.html', context)        

       





#redundant
def index(request):
	albums = Album.objects.all()
	return render(request , 'musicapp/index.html' , {'albums':albums})
    
#redundant
def details(request , album_id):
    #try:
    #	album = Album.objects.get(id = album_id)
    	
    #except Album.DoesNotExist :
    #	raise Http404("Album does not exist")
    album = get_object_or_404(Album , pk = album_id)

    return render(request , 'musicapp/details.html' , {'album' : album})

def details_songs(request ,album_id):    
    
    album = Album.objects.get(id = album_id)
    songs = album.song_set.all()
    return render(request , 'musicapp/details_songs.html' , {'album':album , 'songs' : songs})

def details_favourite(request , album_id):
    album = Album.objects.get(id = album_id)
    songs = album.song_set.all()
    return render(request , 'musicapp/details_favourite.html' , {'album':album ,'songs' : songs}) 

def favourite_form(request , album_id):
    album = Album.objects.get(id = album_id)
    songs = album.song_set.all()
    return render(request , 'musicapp/favourite_form.html' , {'album':album ,'songs':songs})

def favourite_form_update(request , album_id):
    album = Album.objects.get(id = album_id)
    selected_song = album.song_set.get(id = request.POST['song'])
    
    selected_song.is_favourite=True
    selected_song.save()

    return render(request , 'musicapp/details_songs.html' , {'album':album ,'songs' : album.song_set.all()})

def search_song(request):
    query = request.GET['song_title']
    Songs = Song.objects.filter(song_title__icontains = query)# __icontains : thsi basically compares your field value with the given query , and lists out all the data values which contain the given query , being non case sensitive
    #Songs_all = Song.objects.all()
    #for song in Songs_all:
        #if (song.song_title).lower()==(request.POST['song_title']).lower():
            #Songs.append(song)
    return render(request , 'musicapp/index_song.html' , {'Songs':Songs,'query':query})
















    
