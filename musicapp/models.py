from django.db import models
from django.core.urlresolvers import reverse
from django.views.generic.edit import CreateView , UpdateView , DeleteView

# Create your models here.

class Album(models.Model):

	artist = models.CharField(max_length = 100)
	album_title = models.CharField(max_length = 200)
	genre = models.CharField(max_length = 100)
	album_logo = models.FileField()

	def get_absolute_url(self):# whenever an object is created of this class the page will be redirected to 'details' url
		return reverse('details' , kwargs={'pk':self.pk}) #kwargs is a keyword that allows you to handle unnamed functions parameters in advance

	def __str__(self):
		return self.album_title + ' - '+ self.artist

class Song(models.Model):
	album = models.ForeignKey(Album,on_delete = models.CASCADE)
	file = models.FileField()
	song_title = models.CharField(max_length = 100)
	is_favourite = models.BooleanField(default = False)

	def __str__(self):
		return self.song_title

	def get_absolute_url(self):# whenever an object is created of this class the page will be redirected to 'details' url
		return reverse('details' , kwargs={'pk':(self.album).pk})


	


    